bibliotech
---

Hi, and welcome to bibliotech, an effort to learn about the internals of popular javascript libraries.

This is a small repo, that comes with some basic tooling for writing/running your code. (currently - [babel](https://babeljs.io/), [eslint](http://eslint.org/), [mocha](https://mochajs.org/) + [expect](https://github.com/mjackson/expect)).


- fork this repo to your personal account
- run `npm install`
- setup eslint for [your editor of choice](http://eslint.org/docs/user-guide/integrations)


schedule
---

3rd May - [underscore.js](http://underscorejs.org/)